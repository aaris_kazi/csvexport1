-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2021 at 02:33 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vacancies`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs_data`
--

CREATE TABLE `jobs_data` (
  `id` int(4) NOT NULL,
  `country` varchar(32) NOT NULL,
  `pincode` int(6) NOT NULL,
  `url` varchar(50) NOT NULL,
  `designation` varchar(32) NOT NULL,
  `Location` varchar(50) NOT NULL,
  `detectionTime` time NOT NULL,
  `expiryTime` time NOT NULL,
  `isExpired` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs_data`
--

INSERT INTO `jobs_data` (`id`, `country`, `pincode`, `url`, `designation`, `Location`, `detectionTime`, `expiryTime`, `isExpired`) VALUES
(1, 'Belgium', 9000, '', 'Manager', 'Near Brussells', '00:00:00', '00:00:00', ''),
(2, 'Belgium', 2000, '', 'Operator', 'Antwerp', '00:00:00', '00:00:00', ''),
(3, 'Belgium', 1500, '', 'Clerk', 'Flemis Brabant', '00:00:00', '00:00:00', ''),
(4, 'Belgium', 1300, '', 'Technician', 'Walloon Brabant', '00:00:00', '00:00:00', ''),
(5, 'Belgium', 2000, '', 'Material Engineer', 'Antwerp', '00:00:00', '00:00:00', ''),
(9, 'Belgium', 9000, 'http://somesite.com/vacancies', 'manager', 'near Brussels', '12:00:00', '12:00:00', 'yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs_data`
--
ALTER TABLE `jobs_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs_data`
--
ALTER TABLE `jobs_data`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
