<?php  
include 'connect.php'; 
 if(isset($_POST["export"]))  
 {    
      header('Content-Type: text/csv; charset=utf-8');  
      header('Content-Disposition: attachment; filename=data.csv');  
      $output = fopen("php://output", "w");  
      fputcsv($output, array('ID', 'Country', 'URL', 'Zipcode', 'Job Title', 'Location', 'Time Detection', 'Expired Time', 'Is Exported'));  
      $query ="SELECT * FROM jobs_data ORDER BY id"; 
      $result = mysqli_query($connect, $query);  
      while($row = mysqli_fetch_assoc($result))  
      {  
           fputcsv($output, $row);  
      }  
      fclose($output);  
 }  
 ?> 