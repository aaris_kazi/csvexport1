<?php  
include 'connect.php';  
 $query ="SELECT * FROM jobs_data ORDER BY id"; 
 $result = mysqli_query($connect, $query);  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <title>Showing the data</title>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
      </head>  
      <body>  
           <br /><br />  
           <div class="container" style="width:900px;">  
                <!-- <h2 align="center">Export Mysql Table Data to CSV file in PHP</h2>   -->
                <h3 align="center">Jobs Data</h3>                 
                <br />  
                <form method="post" action="export.php" align="center">  
                     <input type="submit" name="export" value="CSV Export" class="btn btn-success" />  
                </form>  
                <br />  
                <div class="table-responsive" id="employee_table">  
                     <table class="table table-bordered">  
                          <tr>  
                               <th width="5%">ID</th>  
                               <th width="25%">Country</th>  
                               <th width="35%">URL</th>  
                               <th width="35%">Zipcode</th>  
                               <th width="10%">Job Title</th>  
                               <th width="20%">Location</th>
                               <th width="20%">Detection Time</th>  
                               <th width="20%">Expired Time</th>  
                               <th width="10%">Is Exported</th>  
                               
                          </tr>  
                     <?php  
                     while($row = mysqli_fetch_array($result))  
                     {  
                     ?>  
                          <tr>  
                               <td><?php echo $row["id"]; ?></td>  
                               <td><?php echo $row["country"]; ?></td>  
                               <td><?php echo $row["pincode"]; ?></td>                                 
                               <td><?php echo $row["url"]; ?></td>                                 
                               <td><?php echo $row["designation"]; ?></td>  
                               <td><?php echo $row["Location"]; ?></td>  
                               <td><?php echo $row["detectionTime"]; ?></td>  
                               <td><?php echo $row["expiryTime"]; ?></td>  
                               <td><?php echo $row["isExpired"]; ?></td>  
                          </tr>  
                     <?php       
                     }  
                     ?>  
                     </table>  
                </div>  
           </div>  
      </body>  
 </html>  